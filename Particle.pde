class Particle {
  PVector pos;
  PVector vel;
  Particle(){
    pos = new PVector(random(1,width),random(1,height));
    vel = PVector.random2D();
  }
}
