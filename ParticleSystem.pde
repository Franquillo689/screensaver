class ParticleSystem {
  
  ArrayList<Particle> particles;
  int len;
  float[][] dist;

  ParticleSystem(int amount){
    len = amount;
    particles = new ArrayList<Particle>();
    dist = new float[len][len];
    for (int i = 0;i < len;i++)
      particles.add(new Particle());
  }
  
  void run(){
    // Updates positions of each particle
    for (int i = 0;i < len;i++){
      Particle p = particles.get(i);
      p.pos.add(p.vel);
      if (p.pos.y >= height || p.pos.y <= 0) p.vel.y *= -1;
      if (p.pos.x >= width  || p.pos.x <= 0) p.vel.x *= -1;
      particles.set(i, p);
    }
    // Calculates distances
    strokeWeight(2);
    int par = 100;
    for (int i = 0;i < len;i++){
      for (int j = 1+i;j < len;j++){
        float dist = particles.get(i).pos.dist(particles.get(j).pos) + 0.001;
        if(dist < par){
          float alpha = map(dist, 0, par, 255, 0);
          stroke(0,255,255,alpha);
          line(particles.get(i).pos.x,particles.get(i).pos.y,particles.get(j).pos.x,particles.get(j).pos.y);
        }
      }
    }
    
    // Draw all the particles
    fill(255);
    noStroke();
    for (Particle p : particles){
      ellipse(p.pos.x, p.pos.y ,5,5);
    }
    
  }
  
}
