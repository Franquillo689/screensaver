
ParticleSystem part;

// if using P2D then uses more RAM, but less CPU
// if not using P2D then uses less RAM, but a lot of CPU
void settings() { 
  System.setProperty("jogl.disable.openglcore", "false");
  fullScreen(P2D);
}

void setup(){
  part = new ParticleSystem(400);
}

void draw(){
  background(0);
  part.run();
}
